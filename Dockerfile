FROM apache/airflow:2.0.1-python3.8
LABEL maintainer="kd2718"

### ROOT UPDATE
USER root

# Never prompt the user for choices on installation/configuration of packages
ENV DEBIAN_FRONTEND noninteractive
ENV TERM linux

# Airflow
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - && \

    #Debian 10
    curl https://packages.microsoft.com/config/debian/10/prod.list > /etc/apt/sources.list.d/mssql-release.list && \

    apt update && \

    ACCEPT_EULA=Y apt-get install msodbcsql17 -yqq && \
    ACCEPT_EULA=Y apt-get install mssql-tools -yqq && \
    apt-get install unixodbc-dev g++ -yqq && \
    apt upgrade -yqq && \
    curl -sL https://aka.ms/InstallAzureCLIDeb | bash

USER airflow
COPY af2_requirements.txt af2_requirements.txt
RUN pip install --upgrade pip setuptools wheel && \
    pip install -r af2_requirements.txt


RUN echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bash_profile && \
    echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc
